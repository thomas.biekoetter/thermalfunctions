from setuptools import setup


with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name = "thermalFunctions",
    version = "1.0.0",
    description = "A python implementation of the thermal functions Jb and Jf.",
    long_description = long_description,
    packages = ["thermalFunctions"],
    author = ["Thomas Biekotter"],
    author_email = [
        "thomas.biekoetter@desy.de"],
    url = "https://gitlab.com/thomas.biekoetter/thermalfunctions",
    include_package_data = True,
    package_data = {"thermalFunctions": ["*"]},
    install_requires = ["numpy"],
    python_requires = '>=3.6',
)
