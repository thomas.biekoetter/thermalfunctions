# thermalFunctions

A python package for the fast evaluation of
the bosonic (Jb) and the fermionic (Jf)
thermal functions occurring in finite
temperature quantum field theory.

The arguments and the signs
of the functions are such that
Jb(x) = J_B(y^2) and Jf(x) = - J_F(y^2)
from [arXiv:1802.02720].

## Author

[Thomas Biekötter]

## Citation guide

If you use this package for a scientific publication,
please consider citing the paper [arXiv:2103.12707], for which
this code was developed.

## Installation:

To install the package just do:

```
make
```

The installation requires a Fortran compiler
and the package numpy. To execute the example
script one additionally needs the packages
matplotlib (for plotting) and cosmoTransitions
(for comparison).

## Disclaimer

Use this package at your own risk. If you encounter
any problems, please raise an issue.


[Thomas Biekötter]: mailto:thomas.biekoetter@desy.de
[arXiv:1802.02720]: https://arxiv.org/abs/1802.02720
[arXiv:2103.12707]: https://arxiv.org/abs/2103.12707
