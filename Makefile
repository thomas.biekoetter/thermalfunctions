.DEFAULT_GOAL = package

PC = python
FC = gfortran

package: build install

pre:
	@$(PC) -m pip install -U --no-cache-dir pip setuptools numpy

build:
	bash -c "cd ./thermalFunctions && PC=$(PC) FC=$(FC) make build"

install:
	@$(PC) -m pip install --no-cache-dir .

clean:
	@echo
	@echo "$(BOLD)Cleaning...$(RESET)"
	bash -c "git clean -f -xdf"
	@echo "$(BOLD)Done...$(RESET)"
