import numpy as np
import matplotlib.pyplot as plt

from thermalFunctions import Jb, Jf
from cosmoTransitions.finiteT import Jb_spline, Jf_spline


def main():

    xb = np.linspace(-10., 10., 1000)
    xf = np.linspace(0., 10., 1000)

    yb = Jb(xb)
    ybc = Jb_spline(xb)
    yf = -Jf(xf)
    yfc = -Jf_spline(xf)

    plt.plot(xb, yb, label='b nd2hm')
    plt.plot(xb, ybc, label='b cosmo spline')
    plt.plot(xf, yf, label='f nd2hm')
    plt.plot(xf, yfc, label='f cosmo spline')

    plt.legend()

    plt.show()


main()
