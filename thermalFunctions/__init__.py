__version__ = "0.1.0"

import numpy as np

from thermalFunctions.Jsplines import jsplines

Jb = np.vectorize(jsplines.jb)

_Jf = np.vectorize(jsplines.jf)
def Jf(x):
    if np.any(x < 0.):
        raise NotImplementedError(
            'Negative arguments for Jf not yet supported.')
    else:
        return _Jf(x)
